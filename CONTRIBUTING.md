There are currently three core components to gmi-web.

- `html.js` gmi-to-html implementation
- `gmi.css` and `gmi-web.css` stylesheets consumed by `css.js`
- gmi-web(1) as defined by `cli.js`

You will need [node(1)](https://nodejs.org/en/) and [scdoc(1)](https://git.sr.ht/~sircmpwn/scdoc) to run `cli.js` and build the `gmi-web.1.scd` man page. `npm link` will install gmi-web(1) from the source allowing you to make modifications locally and test them immediately.

```
git clone https://codeberg.org/talon/gmi-web.git
cd gmi-web
npm link
```

Use `npm run test` to compare your changes to the latest snapshots and when you are ready `npm run test:update` to save the current snapshots. `npm run test:watch` will auto-run the tests interactively to streamline this process.

Patches and issues can be submitted to code@talon.computer or expressed through the channels available on [Codeberg](https://codeberg.org/talon/gmi-web).
