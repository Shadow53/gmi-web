# gmi-web
> a bridge between HTML and Gemini
=> https://codeberg.org/talon/gmi-web
=> https://www.npmjs.com/package/gmi-web-cli on NPM 

## line-types

gmi-web makes writing HTML documents as simple as learning the handful of Gemini line-types:
* paragraphs 
* preformatted blocks 
* lists
* quotes 
* headings 

```if you are familiar with markdown it's kinda like that but even simpler
if you are
   familiar with markdown
it's        kinda like that


but even simpler
```

### inline media
=> video.mp4 video with title prop
=> image.jpg image with title prop
=> audio.mp3 audio with title prop
=> video-with-no-title.mp4 
=> image-with-no-title.jpg
=> audio-with-no-title.mp3
