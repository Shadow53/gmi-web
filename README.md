The converted Gemini document should exist inside the `<body>`. Consider if sharing the page with other HTML to put the core document inside `<main>`. [Each Gemini line-type](https://gemini.circumlunar.space/docs/specification.html#5-3-line-orientation) may be translated using the following guide:

````
<p>           ↔
<a>           ↔  =>
<pre>         ↔  ```
<h[1-3]>      ↔  #[##]
<li>          ↔  *
<blockquote>  ↔  >
````

`<li>` must be wrapped in `<ul>`. Take care to render `<pre>` blocks with their original formatting, _do not_ indent the generated HTML for these tags.

`<a>` tags are categorized as inline which CSS [Normal Flow](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Normal_Flow) presents vertically—Gemini only deals with horizontally flowing content, this can be addressed by using [`display: block;` at the CSS level.](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flow_Layout/Block_and_Inline_Layout_in_Normal_Flow#changing_the_formatting_context_an_element_participates_in)

## optional: inline media

If a URL is consumable by `<img>`, `<audio>` or `<video>` you may insert the respective tag inline, instead of an `<a>`. It's a good idea to also include the `controls` property.

These are categorized as inline just like `<a>` and will need `display: block;` styling. Images and video should also have `max-width: 100%;` so they don't overflow the body.

## `<html>` and `<head>`

When producing a complete and valid HTML5 document the first declaration is the required `<!DOCTYPE html>`. At the root of a document is the `<html>` tag which should have a [`lang` attribute declaring the overall language of the page](https://www.w3.org/International/questions/qa-html-language-declarations) as well as `dir="rtl"` if necessary.

A `<head>` tag with at least the following must be included:

```html
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<title></title>
```

These may also be nice to have:

```html
<meta name="author" content="" />
<meta name="description" content="" />
<link rel="canonical" href="gemini://" />
<meta name="color-scheme" content="dark light" />
```

# gmi.css

`gmi.css` is a maximally miniminal stylesheet that overrides the default CSS [Normal Flow](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Normal_Flow) to render content horizontally in a Gemini way. `gmi-web.css` extends upon that slightly with font and color options. The default values are optimized for readability and mobile-friendliness and may be customized by adding a style property to `<html>`.

```html
<head>
  <!-- assuming you wanna host a copy and link to it... -->
  <link rel="stylesheet" type="text/css" href="gmi-web.css" />
  <meta name="color-scheme" content="dark light" />
  <!-- ... -->
</head>
<html style="--foreground:#555555; --background:#9EEBCF;">
  <!-- ... -->
</html>
```

The `--foreground` and `--background` variables will be inverted when
[prefers-color-scheme](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme) is "dark".

# gmi-web(1)

A command-line utility that pulls all of the above together into a unix-like API for creating HTML from Gemini documents.

```sh
npm install --global gmi-web-cli && gmi-web --help
```

## config

A JSON file can be passed to `--config` for conveniently applying any option without using the command-line flag. For example a `web.json` file with the following contents...

```json
{
  "html": "en",
  "description": true,
  "foreground": "#137752",
  "background": "#F4F4F4"
}
```

...can be used like so:

```sh
gmi-web --config web.json $(find ~/gmi/dst -name '*.gmi')
```

## css

gmi-web ships with `gmi.css` and `gmi-web.css` but it is possible to use your own set of completely custom CSS rules _and_ variables by pointing to a file containing them!

```sh
gmi-web --body --css custom.css < doc.gmi
```

gmi-web auto-detects the need for the `<meta>` color-schemes and allows for using `--inline` to insert the declarations as `style` properties on their respective blocks. Variables defined in `:root` will be picked up and made available for configuration just like `gmi-web.css`.

```
gmi-web [files..]

Convert text/gemini to text/html.

Core:
  --body                                                         [boolean]
  --html, --language, --lang                                      [string]
  --css

HTML:
  --author                                                        [string]
  --description

Media:
  --image                                                          [array]
  --audio                                                          [array]
  --video                                                          [array]

gmi-web.css:
  --foreground                                          [default: "black"]
  --background                                          [default: "white"]
  --body-width                                          [default: "48rem"]
  --hyphens                                            [default: "manual"]
  --serif                               [default: "georgia, times, serif"]
  --sans-serif           [default: "avenir, helvetica, arial, sans-serif"]
  --mono                          [default: "consolas, monaco, monospace"]
  --p-family                                     [default: "var(--serif)"]
  --p-size                                            [default: "1.25rem"]
  --p-height                                              [default: "1.5"]
  --p-indent                                             [default: "0rem"]
  --a-family                                     [default: "var(--serif)"]
  --a-size                                      [default: "var(--p-size)"]
  --a-height                                              [default: "1.5"]
  --a-decoration                                    [default: "underline"]
  --a-style                                            [default: "normal"]
  --pre-family                                    [default: "var(--mono)"]
  --pre-size                                             [default: "1rem"]
  --pre-height                                              [default: "1"]
  --h1-family                               [default: "var(--sans-serif)"]
  --h1-size                                              [default: "3rem"]
  --h1-height                                            [default: "1.25"]
  --h2-family                               [default: "var(--sans-serif)"]
  --h2-size                                           [default: "2.25rem"]
  --h2-height                                            [default: "1.25"]
  --h3-family                               [default: "var(--sans-serif)"]
  --h3-size                                            [default: "1.5rem"]
  --h3-height                                            [default: "1.25"]
  --ul-family                                    [default: "var(--serif)"]
  --ul-size                                     [default: "var(--p-size)"]
  --ul-height                                            [default: "1.25"]
  --ul-style                                           [default: "circle"]
  --quote-family                                 [default: "var(--serif)"]
  --quote-size                                  [default: "var(--p-size)"]
  --quote-height                                         [default: "1.25"]
  --quote-style                                        [default: "italic"]

Options:
  --version  Show version number                                 [boolean]
  --config   Path to JSON config file
  --help     Show help                                           [boolean]

Examples:
  gmi-web --body < doc.gmi
  gmi-web --html en $(find ~/my-capsule -name '*.gmi')
  gmi-web --foreground '#9EEBCF' --html en < doc.gmi
  gmi-web --image jpg --audio mp3 --image png --body < doc.gmi

See the gmi-web(1) man page for more information.
```

# license

gmi-web is free and unencumbered public domain software. For more information, see http://unlicense.org/ or the accompanying UNLICENSE file.
